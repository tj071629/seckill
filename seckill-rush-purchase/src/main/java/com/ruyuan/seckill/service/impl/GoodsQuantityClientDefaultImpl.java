package com.ruyuan.seckill.service.impl;

import com.ruyuan.seckill.domain.vo.GoodsQuantityVO;
import com.ruyuan.seckill.service.GoodsQuantityClient;
import com.ruyuan.seckill.service.GoodsQuantityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 库存操作实现
 */
@Service
public class GoodsQuantityClientDefaultImpl implements GoodsQuantityClient {

    @Autowired
    private GoodsQuantityManager goodsQuantityManager;



    /**
     * 秒杀库存更新接口
     *
     * @param goodsQuantityList 要更新的库存vo List
     * @return 如果更新成功返回真，否则返回假
     */
    @Override
    public Boolean updateSeckillSkuQuantity(List<GoodsQuantityVO> goodsQuantityList) {
        return goodsQuantityManager.updateSeckillSkuQuantity(goodsQuantityList);
    }


}
