package com.ruyuan.seckill.service.cartbuilder;

import com.ruyuan.seckill.domain.vo.CartSkuVO;
import com.ruyuan.seckill.domain.vo.PromotionScriptVO;

import java.util.List;
import java.util.Map;

/**
 * 脚本处理
 **/
public interface ScriptProcess {
    /**
     * 计算促销优惠价格
     *
     * @param script 脚本
     * @param params 参数
     * @return 优惠后的价格
     */
    Double countPrice(String script, Map<String, Object> params);

    /**
     * 获取购物车级别促销脚本信息
     *
     * @param sellerId
     * @return
     */
    List<PromotionScriptVO> readCartScript(Integer sellerId);

    /**
     * 获取运费
     *
     * @param script 脚本
     * @param params 参数
     * @return 运费
     */
    Double getShipPrice(String script, Map<String, Object> params);

    /**
     * 获取sku促销活动信息
     *
     * @param skuId 货品id
     * @return
     */
    List<PromotionScriptVO> readSkuScript(Integer skuId);

    /**
     * 获取sku级别促销脚本信息
     *
     * @param skuList
     * @return
     */
    List<PromotionScriptVO> readSkuScript(List<CartSkuVO> skuList);

    /**
     * 获取活动是否有效
     *
     * @param script
     * @return true：活动有效，false:活动无效
     */
    Boolean validTime(String script);

    /**
     * 读取优惠券脚本
     *
     * @param couponId 优惠券id
     * @return
     */
    String readCouponScript(Integer couponId);
}
