package com.ruyuan.seckill.domain.vo;

import java.util.List;

/**
 * 优惠券是否可用的验证结果
 */
public class CouponValidateResult {

    /**
     * skuid
     */
    private List<Integer> skuIdList;

    /**
     * 优惠券是否可用
     */
    private boolean isEnable;

    public List<Integer> getSkuIdList() {
        return skuIdList;
    }

    public void setSkuIdList(List<Integer> skuIdList) {
        this.skuIdList = skuIdList;
    }

    public boolean isEnable() {
        return isEnable;
    }

    public void setEnable(boolean enable) {
        isEnable = enable;
    }
}
