package com.ruyuan.seckill.domain.enums;


public enum PromotionTarget {

    CART,

    SKU

}
